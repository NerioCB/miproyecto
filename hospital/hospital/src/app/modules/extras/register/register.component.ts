import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UsuarioService } from "src/app/core/services/usuario.service";
import { Router } from "@angular/router";
import { LocalStorageService } from "src/app/core/services/local-storage.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public submitted: Boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private usuarioService: UsuarioService,
    private storage: LocalStorageService
  ) {}

  sonIguales(x1: string, x2: string) {
    return (group: FormGroup) => {
      let c1 = group.controls[x1].value;
      let c2 = group.controls[x2].value;

      console.log(c1, "c1");
      console.log(c2, "c2");

      if (x1 === x2) return true;
      else return null;
    };
  }

  ngOnInit() {
    this.storage.setTocken("");
    this.registerForm = this.fb.group(
      {
        name: ["", [Validators.required]],
        email: ["", [Validators.required, Validators.email]],
        password: ["", [Validators.required]],
        password2: ["", Validators.required],
        condition: [""]
      }
      // { validators: this.sonIguales("password", "password2") }
    );
  }

  get c() {
    return this.registerForm.controls;
  }

  saveRegister() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return false;
    }
    this.submitted = false;
    const values = this.registerForm.value;
    const model = {
      nombre: values.name,
      email: values.email,
      img: null,
      password: values.password,
      role: "ADMIN_ROLE"
    };
    this.usuarioService.postUsuarioService(model).subscribe((res: any) => {
      console.log(res, "response data");
      if (res.success) {
        this.router.navigate(["authorization/login"]);
      }
    });
  }
}
