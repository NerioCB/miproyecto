import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UsuarioService } from "src/app/core/services/usuario.service";
import { Usuario } from "src/app/models/Usuario";
import { LocalStorageService } from "src/app/core/services/local-storage.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private storage: LocalStorageService
  ) {}

  ngOnInit() {
    this.storage.setTocken("");
    this.loginForm = this.fb.group({
      name: ["", Validators.required],
      password: ["", Validators.required],
      remember: [false]
    });
  }

  loginUser() {
    console.log(this.loginForm.value, "this.loginForm.value");
    if (this.loginForm.invalid) {
      return false;
    }
    const value = this.loginForm.value;
    const user: Usuario = {
      nombre: "",
      email: value.name,
      password: value.password
    };

    this.usuarioService.login(user, value.remember).subscribe((res: any) => {
      if (res.success) {
        this.storage.setTocken(res.token);
        this.router.navigate(["dashboard/home"]);
      }
    });
  }
}
