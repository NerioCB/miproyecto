import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TOKEN } from 'src/app/config/config';
import { Usuario } from 'src/app/models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  constructor(private http: HttpClient) { }


  login(usuario: Usuario, remember) {
    return this.http.post('login', usuario);
  }


  // getUsuarioService() {

  //   const params: HttpParams = new HttpParams()
  //     .set('index', '0')
  //     .set('limit', '100');

  //   return this.http.get('Usuario', { params: params });
  // }

  postUsuarioService(model: any) {
    return this.http.post('usuario', model);
  }

  // putUsuarioService(id: number, model) {
  //   const params: HttpParams = new HttpParams()
  //   .set('token', TOKEN);

  //   return this.http.put(`Usuario/${id}`, model, { params: params});
  // }

  // deleteUsuarioService(id: number) {
  //   const params: HttpParams = new HttpParams()
  //   .set('token', TOKEN);

  //   return this.http.delete(`Usuario/${id}`, {params: params});
  // }
}
