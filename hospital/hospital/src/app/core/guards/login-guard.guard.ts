import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { LocalStorageService } from "../services/local-storage.service";

@Injectable({ providedIn: "root" })
export class LoginGuard implements CanActivate {
  constructor(private storage: LocalStorageService,
    private router: Router,) {}

  canActivate(): boolean {
    const value = this.storage.getToken();
    console.log(value, 'value tocken ');
    if (value) {
      return true;
    } else {
      this.router.navigate(["authorization/login"]);
      return false;
    }
  }
}
