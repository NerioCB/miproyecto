var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var app = express();
var Usuario = require('../models/usuario');

// cosnt
var SEDD = require('../config/config').SEED;


const { OAuth2Client } = require('google-auth-library');
var CLIENT_ID = require('../config/config').CLIENT_ID;
const client = new OAuth2Client(CLIENT_ID);


async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();
    // const userid = payload['sub'];
    // If request specified a G Suite domain:
    //const domain = payload['hd'];
    return {
        nombre: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    }
}

//================================
//Authentication
//================================

app.post('/google', async(req, res) => {

    var token = req.body.token;

    var googleUser = await verify(token).catch(e => {
        res.status(403).json({
            success: false,
            message: 'Token no valido'
        });
    });

    Usuario.find({ email: googleUser.email }, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                success: false,
                message: 'Error al buscar el usuario',
                errors: error
            });
        }

        if (usuarioDB) {
            if (usuarioDB.google) {
                return res.status(400).json({
                    success: false,
                    message: 'Debe de authenticarse con google',
                });
            } else {
                var token = jwt.sign({ usuario: usuario }, SEDD, { expiresIn: 14400 }); // 4 horas
                res.status(200).json({
                    success: true,
                    token: token,
                    usuario: usuario,
                });
            }
        } else { // the user not exist
            var usuario = new Usuario();
            usuario.nombre = googleUser.nombre;
            usuario.email = googleUser.email;
            usuario.img = googleUser.img;
            usuario.google = true;
            usuario.password = ':)';

            usuario.save(err, usuarioDB => {
                if (err) {
                    return res.status(500).json({
                        success: false,
                        message: 'Error al buscar el usuario',
                        errors: error
                    });
                }
                var token = jwt.sign({ usuario: usuario }, SEDD, { expiresIn: 14400 }); // 4 horas
                res.status(200).json({
                    success: true,
                    token: token,
                    usuario: usuario,
                });
            });
        }
    });


    // res.status(200).json({
    //     success: true,
    //     message: 'Ok',
    //     googleUser: googleUser
    // });
});

//=======================

app.post('/', (req, res) => {

    var body = req.body;

    Usuario.findOne({ email: body.email }, (err, usuario) => {
        if (err) {
            return res.status(500).json({
                success: false,
                message: 'Error al buscar el usuario',
                errors: error
            });
        }
        if (!usuario) {
            return res.status(400).json({
                success: false,
                message: 'No existe el usuario con el email: ' + body.email,
                errors: err
            });
        }

        if (!bcrypt.compareSync(body.password, usuario.password)) {
            return res.status(400).json({
                success: false,
                message: 'No existe el usuario con el - password ',
                errors: err
            });
        }

        // crear un token
        usuario.password = ':)';
        var token = jwt.sign({ usuario: usuario }, SEDD, { expiresIn: 14400 }); // 4 horas
        res.status(200).json({
            success: true,
            token: token,
            usuario: usuario,
        });
    });
});

module.exports = app;